function [z] = gausspdf(x,mu,sigma);

% GAUSSPDF  Values of the Gaussian probability density
%           function
%
%    GAUSSPDF(X,MU,SIGMA) returns the likelihood
%    of the point or set of points X with respect to
%    a Gaussian process with mean MU and covariance
%    SIGMA. MU is a 1*D vector (where D is the dimension
%    of the process), SIGMA is a D*D matrix
%    and X is a N*D matrix where each row is a
%    D-dimensional point.
%
%    See also MEAN, COV, GLOGLIKE

[N,D] = size(x);
%%%%D
if (min(N,D) == 1), x=x(:)'; end;
fprintf(1,'la valeur du X');
%%%%%x
%%%%fprintf(1,'la valeur du l''inverse de Sigma');
invSig = inv(sigma);
%%%%%%invSig
%invSig
%%%%%%%fprintf(1,'la valeur de mu');
mu = mu(:)';
%%%%%%%mu
%mu
%pause
x = x-repmat(mu,N,1);
%%%%%fprintf(1,'la valeur du X - mu');
%%%%%%x
z = sum( ((x*invSig).*x), 2 );
%%%%%fprintf(1,'la valeur de (x-mu) * inverse de Sigma * (x - mu)');
%%%%%%z
%%%%%%fprintf(1,'la valeur de sqrt((2*pi)^D * det(sigma)');
a=sqrt((2*pi)^D * det(sigma));
%%%%%%%a
z = exp(-0.5*z) / sqrt((2*pi)^D * det(sigma));
%%%%%%%%fprintf(1,'la valeur de exp(-0.5*z) / sqrt((2*pi)^D * det(sigma))');
%%%%%%%%%%z
z