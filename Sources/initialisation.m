%%%%%%%%%%This function is written by IBN KHEDHER Mohamed%%%%%
%%%%%%%%%%%%%%%%Telecom SUD Paris%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%Copy Right%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%Juin 2011%%%%%%%%%%%%%%%%%%%%%%%%
function [mu0,Sigma0]=initialisation(Q,M,data,cov_type,PR )
[mu0, Sigma0] = mixgauss_init(Q*M, data, cov_type);
mu0 = reshape(mu0, [PR Q M]);
Sigma0 = reshape(Sigma0, [PR PR Q M]);
end