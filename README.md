# Lab: Action Recognition from vido sequences 


## _Action Recognition:_ Human Action Recognition from video sequences.
This work was achived during the PhD of Mohamed IBN KHEDHER, supervised by Pr. Mounim El-Yacoubi and Pr. Bernadette Dorizzi

This repository contains a MATLAB implementation of Human Action Recognition (HAR) from video sequences , based on Hidden Markov Model (HMM) as classifier and Histogram Of Gradiend (HOG) and Histogram of Optical Flow (HOF) as features.
    
    @inproceedings{Khedher2012,
        Author = {Mohamed Ibn Khedher and Mounim A. El{-}Yacoubi and Bernadette Dorizzi},
        title     = {Human Action Recognition using Continuous HMMs and {HOG/HOF} Silhouette Representation},
        booktitle = {{ICPRAM} 2012 - Proceedings of the 1st International Conference on Pattern Recognition Applications and Methods, Volume 2, Vilamoura, Algarve, Portugal, 6-8 February, 2012},
        year      = {2012},
    }

## Installation:

The following instructions are written for Windows-based MATLAB. For Linux-based MATLAB, all paths should be adapted to Linux Syntax.

- Clone the Action Recognition repository:

  ```Shell
  git clone https://gitlab.com/Khedher/Action-Recognition.git 
  ```
  If you have not ``git`` installed on your system, download directory the repository from the website
  
  Let's call the top level directory of the Action Recognition Lab `HAR_ROOT`. 

-  Setup your path.

  1. Displace on root Path.
  
    ```Matlab
    cd HAR_ROOT
    ```
    
  2. Add path of Librairies.
  
    ```Matlab
    addpath(genpath(HAR_ROOT))
    ```
## Demo:

This code include 2 Demo:
 1. Using pre-extracted features.
 2. On line extracted features .

The HAR system includes three main steps:
 1. Features Exraction.
 2. Projection on the space PCA (Principal Component Analysis).
 3.  HMM Training.
 4.  Test on the database KTH.

- Now we can run the demo. 

## I) Using pre-extracted features:
 
- To run the demo,

  ```matlab
  cd HAR_ROOT
  main_using_preExtracted_Features.m
  ```

## II) On line extracted features:
 
 
- Download the KTH human action dataset: [videos](https://drive.google.com/open?id=1TBglOcFKY8N_QdKkbWKwjjcFi2TJmoJ9). Put them under `HAR_root`. Unzip them, then you will get 6 directories.

When this step is finished, the structure of `HAR_ROOT\` should contain:

  ```matlab
  HAR_ROOT/KTH/
                    |->boxing/
                    |     |-> person01_boxing_d1_uncomp.avi
                    |     |-> **************
                    |     L-> person25_boxing_d4_uncomp.avi
                    |->handclapping/
                    |     |-> person01_handclapping_d1_uncomp.avi
                    |     |-> **************
                    |     L-> person25_handclapping_d4_uncomp.avi
                    |->handwaving/
                    |     |-> person01_handwaving_d1_uncomp.avi
                    |     |-> **************
                    |     L-> person25_handwaving_d4_uncomp.avi
                    |->jogging/
                    |     |-> person01_jogging_d1_uncomp.avi
                    |     |-> **************
                    |     L-> person25_jogging_d4_uncomp.avi
                    |->running/
                    |     |-> person01_running_d1_uncomp.avi
                    |     |-> **************
                    |     L-> person25_running_d4_uncomp.avi
                    L->walking/
                    |     |-> person01_walking_d1_uncomp.avi
                    |     |-> **************
                    |     L-> person25_walking_d4_uncomp.avi
  ```
  
## LAB

The goal of this project lab is to experiment with a video-based human action recognition system using Hidden Markov Models (HMM). The system consists of the following steps:

* Feature Extraction: 3 options:
  * HOG
  * HOF
  * HOG/HOF combination
* Dimension Reduction based on Principal Component Analysis (PCA)
* HMM Parameter Estimation (HMM training)
* Classification of test video sequences based on HMMs

All the source code is included. No HMM implementation is required. When you type the run button on main_xxxxxx.m, all the steps above will be executed with the current configuration parameters:

* The Region of Interest (ROI) image is subdivided into 3x3 cells, represented each by a histogram vector of dimension 9. Thus, HOG and HOF have each a dimension of 81.
* Dimension reduction by PCA is performed so as 90% variance is kept on the new reduced space.
* The number of states of HMMs is _Q_.
* The number of Gaussians for each state is _M_.

After running this program, you will obtain the recognition rate along with the confusion matrix. Once you get these results, you need to carry out the following tasks:

1.  Analyze and interpret the obtained results obtained with the standard settings below.

    _i_. Features: HOG_HOF
    
    _ii_. _Q_= 5
    
    _iii_. _M_ = 3

2. Modify hyper parameters like Q, M, type of feature, etc., used so as to improve the results. Make an analysis and interpretation in light of the new results.
After completing your lab, send me a report with the following items:

    _i_. Explain briefly the overall system
    
    _ii_. Analyze and interpret the results of the system under the standard settings
    
    _iii_. Analyze and interpret the results of the system after tuning some hyper-parameters.
