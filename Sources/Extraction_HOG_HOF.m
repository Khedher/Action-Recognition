%%%%%%%%%%%%%%%%  HOG/HOFFs Extraction %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%% Optical FLow Toolbox
addpath(genpath('..\toolbox\piotr_toolbox_V2.60'));

%%%%% Database:  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Database: KTH (25 people and 6 actions and 4 scenarios)
video='..\KTH\';

%%%%%% Directory for saving Regions of Interest (ROIs)
mkdir('..\BoundBox');
 
%%%%%% People and actions of Weizmann
action={'boxing','handclapping','handwaving','jogging','running','walking'};


indice=1;
nbvis=4;
for k=1:size(action,2)
     fprintf(' Extraction of HOG/HOF Features from video actions: %s  ... (25 Persons) \n ',action{k});
    for j=unique([ScenarioTraining ScenarioTesting])
        fprintf(' \t \t Scenario: %d\n ',j);
        fprintf(' \t \t  Persons: ');
        for i=1:25
            fprintf('%d ',i);
            if (i<10) 
                file=['person0',int2str(i),'_',action{k},'_d',int2str(j),'_uncomp']; 
            else
                file=['person',int2str(i),'_',action{k},'_d',int2str(j),'_uncomp'];
            end;
            if(show) mkdir(['..\',HOF_Features,action{k},'\',int2str(i),'\','Scenario',int2str(j),'\']); end;
            if(show) mkdir(['..\',HOG_Features,action{k},'\',int2str(i),'\','Scenario',int2str(j),'\']); end;

            file_video=[video,action{k},'\',file,'.avi'];
            if(show) mkdir(['..\BoundBox','\',action{k},'\',int2str(i),'\','Scenario',int2str(j)]); end;
            VID=mmreader(file_video);
            VIDEO=read(VID);
            
            count1=1;
            count2=1;
            countROI=1;
            for l=1:length(struct2cell(info.(action{k}).(['person',int2str(i)]).(['dir',int2str(j)])))
                A=info.(action{k}).(['person',int2str(i)]).(['dir',int2str(j)]).(['Seq',int2str(l)]);
                debut=A(5);
                fin=A(6);
                pt=6;
                Primitive=[];
                precFrame=rgb2gray(VIDEO(:,:,:,debut));
                for ii=debut:fin
                    ImageRGB=VIDEO(:,:,:,ii);
                    curFrame=rgb2gray(ImageRGB);
                    imagef=curFrame(A(pt+1):A(pt+3),A(pt+2):A(pt+4));
                    if((countROI<nbvis) && show)
                        imwrite(imagef,['..\BoundBox','\',action{k},'\',int2str(i),'\','Scenario',int2str(j),'\',int2str(ii),'.png'])  
                        countROI=countROI+1;
                    end
                    %%% HOF/HOG Extraction and Features saving                   
                    if(show)
                        if(count1<nbvis)
                            PrimitiveHOG=HOG(imagef,nx,ny,nbBins,show);
                            saveas(gca,['..\',HOG_Features,action{k},'\',int2str(i),'\','Scenario',int2str(j),'\',int2str(ii),'.png'],'png');
                            close(gcf);
                            count1=count1+1;
                        else
                            PrimitiveHOG=HOG(imagef,nx,ny,nbBins,0);
                        end
                    else
                        PrimitiveHOG=HOG(imagef,nx,ny,nbBins,0);                        
                    end
                    
                    if(show)
                        if(count2<nbvis)
                            PrimitiveHOF=HOF(curFrame,precFrame,nx,ny,nbBins,A(pt+1),A(pt+3),A(pt+2),A(pt+4),show);
                            saveas(gca,['..\',HOF_Features,action{k},'\',int2str(i),'\','Scenario',int2str(j),'\',int2str(ii),'.png'],'png');
                            close(gcf);
                            count2=count2+1;
                        else
                            PrimitiveHOF=HOF(curFrame,precFrame,nx,ny,nbBins,A(pt+1),A(pt+3),A(pt+2),A(pt+4),0);
                        end
                    else
                        PrimitiveHOF=HOF(curFrame,precFrame,nx,ny,nbBins,A(pt+1),A(pt+3),A(pt+2),A(pt+4),0);
                    end
                                                            
                    Primitive=[Primitive , [PrimitiveHOG ;PrimitiveHOF]];
                    pt=pt+4;
                    precFrame=curFrame;
                end
                Desc.(action{k}).(['person',int2str(i)]).(['dir',int2str(j)]).(['Seq',int2str(l)])=Primitive;
            end
        end
        fprintf(' \n');
    end
end
%%
save(['..\Descriptors\Descripteur_HOG_HOF_',num2str(nx),'_',num2str(ny),'_',num2str(nbBins),'.mat'],'Desc');

