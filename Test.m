%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%% HMMs Test %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Number of test sequences 
NumbSeqTest=size(action,2);
%% load HMMs and PCA parameters
load(['./Param/',ParamHMM,'State',num2str(Q),'Mixture',num2str(M),'Cov',cov_type,'_',num2str(nx),'_',num2str(ny),'_',num2str(nbBins),'.mat']);
load(['./Param/','ParamPCA_',num2str(nx),'_',num2str(ny),'_',num2str(nbBins),'_NumEigenVector_',num2str(numRetainedEigenVectors),'.mat']);
%% For a beauty display
actionb={'boxing      ','handclapping','handwaving  ','jogging     ','running     ','walking     '};
% %%% Display Test Protocol
% if LeaveOneOut
%     fprintf(1, '\t \t Test Protocol: Leave One Out. \n \n');
% else
%     fprintf(1, '\t \t Test Protocol: One person for the test / Others for training. \n \n');
% end
nbActionsReconnues=0;
TauxIdentif=[];
for k=ScenarioTesting
    fprintf(1,'Scenario: %d \n',k);
    fprintf(1,' \t Persons:');
for numPersonneTest=testPersonId
    fprintf(1,' %d ',numPersonneTest);

    %% Action recognition
    for j=1:NumbSeqTest
        %% load data of action 'jj' associated with the personn 'testPersonId' (test sequence)
        for seqq=1:length(struct2cell(info.(action{j}).(['person',int2str(numPersonneTest)]).(['dir',int2str(k)])))
            data=Desc.(action{j}).(['person',int2str(numPersonneTest)]).(['dir',int2str(k)]).(['Seq',int2str(seqq)])(:,2:end)';
            %% Projection on the space PCA
            clear vectPP;
            vectPP=ParametersACP.vectP;
            vectPCA=data*vectPP(:,1: numRetainedEigenVectors);
            dataTest{seqq}=vectPCA';
        end
        %% Compute likelihoods between the test sequence and each learned HMM 
        Vraisemblance=[]; 
        for i=1:size(action,2)
            loglik = mhmm_logprob(dataTest, Parameters.(action{i}).prior1, Parameters.(action{i}).transmat1, Parameters.(action{i}).mu1, Parameters.(action{i}).Sigma1, Parameters.(action{i}).mixmat1);
            Vraisemblance=[Vraisemblance,loglik];      
        end
        %% Seek action which maximizes the likelihood
        [maximum,indice]=max(Vraisemblance);
        %   fprintf(1,'La sequence test de l''action %s est reconnu comme l''action %s \n\n\n\n',action{j},action{indice});
        %% Incrementing the number of correctly recognized actions
        if(indice==j )
            nbActionsReconnues=nbActionsReconnues+1;
        end
        MatriceConfusion(j,indice)=MatriceConfusion(j,indice)+1;
    end
end
fprintf(1,'\n');
%TauxIdentif=[TauxIdentif (nbActionsReconnues/(NumbSeqTest*length(testPersonId)))*100];
end
fprintf(1, '------------------------------------------------------------------------------------\n\n');

%% Affichage de la matrice de confusion
fprintf(1, 'Results: \n \n');

fprintf(1, '\t  1) Confusion matrix: \n \n');
fprintf(1,'%s\t','                ');
for i=1:size(MatriceConfusion,1)
    fprintf(1,'%s\t',actionb{i});
end
fprintf('\n');
for i=1:size(MatriceConfusion,1)
    fprintf(1,'%s\t',actionb{i});
    for j=1:size(MatriceConfusion,2)
        fprintf('\t%.2f \t\t',(MatriceConfusion(i,j)/(length(testPersonId)*length(ScenarioTesting)))*100);
    end
    fprintf('\n');
end
fprintf('\n');
%% Taux de reconnaissance
fprintf('\t 2) Recognition Rate = %.2f%% \n ',(nbActionsReconnues/(NumbSeqTest*length(testPersonId)*length(ScenarioTesting)))*100);
