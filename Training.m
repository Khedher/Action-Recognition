%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%% HMMs Parameters Learning %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%  NB: one HMM is learned for each action %%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% For saving HMMs Parameters
ParamHMM='ParamHMMs';

%% HMMs Parameters initialization
prior0 = normalise(rand(Q,1)); %%% prior probabilities
transmat0 =  mk_leftright_transmat(Q,1/2); %%% transmission probabilities
transmat0(Q,Q)=0.5;
transmat0(Q,1)=0.5;

%% HMMs Training

%fprintf(1, '\t Training, in progress ...\n');
%%

data=[];
for j=1:size(action,2)
    fprintf(1, '\t \t HMM Training for the action %s \n',action{j});
    %%% Concatenate the reference datat of the action 'j' in
    %%% the vector 'ActionTrainVector'
    ActionTrainVector={};
    l=0; 
    for i=setdiff(1:maxTrainPersonId,testPersonId)
        for k=ScenarioTraining
            for seqq=1:length(struct2cell(info.(action{j}).(['person',int2str(i)]).(['dir',int2str(k)])))
                l=l+1;
                vect=DescPCA.(action{j}).(['person',int2str(i)]).(['dir',int2str(k)]).(['Seq',int2str(seqq)])(:,:);
                ActionTrainVector{l}=vect;
            end
        end
    end
        data.(action{j})=ActionTrainVector;

    %%  observations probabilities initialization: 'mu0', 'sigma0' and Gaussian Weights 'mixmat0'
    fprintf(1, '\t\t \t HMMs Parameters Initialization \n');
    [mu0,Sigma0]=initialisation(Q,M,data.(action{j}),cov_type,numRetainedEigenVectors);
    mixmat0 = mk_stochastic(rand(Q,M)); 
    
    %% HMMs Parameters Re-estimation
    fprintf(1, '\t\t \t HMMs Parameters Re-estimation: 10 iterations (or converged) \n');
    [LL, prior1, transmat1, mu1, Sigma1,mixmat1] = mhmm_em(data.(action{j}), prior0, transmat0, mu0, Sigma0, mixmat0,'max_iter', 10,'cov_type',cov_type);
   
    %% Saving HMMs Parameters of the action 'j'
    fprintf(1, '\t\t \t Saving Parameters of %s HMM \n \n',action{j});
    Parameters.(action{j}).prior1=prior1;
    Parameters.(action{j}).transmat1=transmat1;
    Parameters.(action{j}).mu1=mu1;
    Parameters.(action{j}).Sigma1=Sigma1; 
    Parameters.(action{j}).mixmat1=mixmat1;     
end
 
save([RepParam,ParamHMM,'State',num2str(Q),'Mixture',num2str(M),'Cov',cov_type,'_',num2str(nx),'_',num2str(ny),'_',num2str(nbBins),'.mat'],'Parameters');




