%%%%%%%%%%%%%%%%  HOGs Extraction %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%% Database:  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Database: KTH (25 people and 6 actions and 4 scenarios)
video='..\KTH\';

%%%%%% Directory for saving Regions of Interest (ROIs)
mkdir('..\BoundBox');
 
%%%%%% People and actions of Weizmann
action={'boxing','handclapping','handwaving','jogging','running','walking'};


indice=1;
nbvis=3;
for k=1:size(action,2)
    fprintf(' Extraction of HOG Features from video actions: %s  ... (25 Persons) \n ',action{k});
     for j=unique([ScenarioTraining ScenarioTesting])
         fprintf(' \t \t Scenario: %d\n ',j);
         fprintf(' \t \t  Persons: ');
        for i=1:25
           fprintf('%d ',i);
            if (i<10) 
                file=['person0',int2str(i),'_',action{k},'_d',int2str(j),'_uncomp']; 
            else
                file=['person',int2str(i),'_',action{k},'_d',int2str(j),'_uncomp'];
            end;
            if(show) mkdir(['..\',HOG_Features,action{k},'\',int2str(i),'\','Scenario',int2str(j),'\']); end;
            file_video=[video,action{k},'\',file,'.avi'];
           if(show) mkdir(['..\BoundBox','\',action{k},'\',int2str(i),'\','Scenario',int2str(j)]); end;
            VID=mmreader(file_video);
            VIDEO=read(VID);
            
            count=1;
            countROI=1;
            for l=1:length(struct2cell(info.(action{k}).(['person',int2str(i)]).(['dir',int2str(j)])))
                A=info.(action{k}).(['person',int2str(i)]).(['dir',int2str(j)]).(['Seq',int2str(l)]);
                debut=A(5);
                fin=A(6);
                pt=6;
                Primitive=[];
                for ii=debut:fin
                    ImageRGB=VIDEO(:,:,:,ii);
                    ImgaeGray=rgb2gray(ImageRGB);
                    imagef=ImgaeGray(A(pt+1):A(pt+3),A(pt+2):A(pt+4));
                    if((countROI<nbvis) && show)
                        imwrite(imagef,['..\BoundBox','\',action{k},'\',int2str(i),'\','Scenario',int2str(j),'\',int2str(ii),'.png'])
                        countROI=countROI+1;
                    end
                    if(show)
                        if(count<nbvis)
                            Primitive=[Primitive , HOG(imagef,nx,ny,nbBins,show)];
                            saveas(gca,['..\',HOG_Features,action{k},'\',int2str(i),'\','Scenario',int2str(j),'\',int2str(ii),'.png'],'png');
                            close(gcf);
                            count=count+1;
                        else
                            Primitive=[Primitive , HOG(imagef,nx,ny,nbBins,0)];
                        end
                    else
                        Primitive=[Primitive , HOG(imagef,nx,ny,nbBins,0)];                            
                    end
                    
                    pt=pt+4;
                end
                Desc.(action{k}).(['person',int2str(i)]).(['dir',int2str(j)]).(['Seq',int2str(l)])=Primitive;
            end
        end
        fprintf(' \n');
    end
end
%%
save(['..\Descriptors\DescripteurHOG_',num2str(nx),'_',num2str(ny),'_',num2str(nbBins),'.mat'],'Desc');



