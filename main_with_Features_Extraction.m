%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%% TP on Human Actions Recognition (KTH Database) %%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all
warning off;  clc;
%%% The system includes three main steps:
%%% 1) Features Exraction
%%% 2) Projection on the space PCA (Principal Component Analysis)
%%% 3) HMM Training
%%% 4) Test on the database KTH
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%    Parameters Initialization (Lines 18-58) %%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% 1) Features Exraction (HOG or HOF HOG/HOF): HOG ( Histograms of Oriented (spatial) Gradient), HOF (Histograms of Optical Flow).
    %%% Histogram (HOG or HOF HOG/HOF) for each region of the image  
    %%% --> divide the image into a grid of regions of size (nx, ny)
    %%% compute a histogram, of 'nbBins' bins, for each region 
    %% HOG or HOF or HOG/HOF
    %% MAKE SURE THAT ONLY ONE FEATUERE IS EQUAL TO "1", THE OTHERS ARE EQUAL TO "0"
    HOG_=0;
    HOF_=0;
    HOG_HOF_=1;
    %% Saving Features and Regions Of Interest (ROI)
    show=0;
%%  Features Exraction Parameters
    nx=3;
    ny=3;
    nbBins=9;

%% 2) Projection on the space PCA 
    %%%% Initialize the variance percentage (between 1 and 100) retained by the PCA
    retainedVariancePercentage=90; 
%     if(numRetainedEigenVectors>(nx*ny*nbBins))
%         msgbox('Retained Eigenvectors mus be less then the initil dimension!', 'Error','error');
%         return;
%     end
    
%% 3) HMM Training
    %HMM parameter initialization:
    M =3;          %Number of mixtures 
    Q =5;          %Number of states 
    cov_type = 'diag';%Covariance type 'diag' or 'full'
    %Scenario for training
    %Choose Person identities using for traning
    %People with identities from 1 to "maxTrainPersonId" are used for training
    maxTrainPersonId=16; 
    ScenarioTraining=[1 2 3 4];%1 and/or 2 and/or 3 and/or 4
 
%%  4) Test Protocol: 
      %HMM is learned for each action: people having the identity
         %  testPersonId is used for testing, the others for training.
            testPersonId=[17 18 19 20 21 22 23 24 25];% 
         %Scenario for testing
            ScenarioTesting=[1 2 3 4];%1 and/or 2 and/or 3 and/or 4
         
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% HMMs Toolbox:
addpath(genpath('./toolbox/HMMall/'));
%% Fichiers Sources:
addpath(genpath('./Sources/'));
%% For saving features
HOG_Features='HOG_Features/'; mkdir(HOG_Features);
HOF_Features='HOF_Features/'; mkdir(HOF_Features);

%% For saving HMM and PCA parameters
    RepParam ='./Param/';
    mkdir(RepParam);


%% Database
%%% Actions and people of the databse KTH:
%personne={'daria','denis','eli','ido','ira','lena','lyova','moshe','shahar'};
action={'boxing','handclapping','handwaving','jogging','running','walking'};
fprintf(1, '----------------------------------------------------------------------------------------\n');     
fprintf(1, '\t \t \t \t \t  \t \t KTH Database: \n \n');
fprintf(1, '\t \t \t Actions: boxing, handclapping, handwaving, jogging, running, walking. \n');
fprintf(1, '\t \t \t 25 Persons \n');
 fprintf(1, '--------------------------------------------------------------------------------------\n\n');     
%%%%%%%%%%%%%%%%%%%% Silhouettes Info Loading  %%%%%%%%%%%%%%%%%%%%%%%
load('.\KTHBoundingBoxInfo.mat');

%% Confusion Matrix
MatriceConfusion=zeros(size(action,2),size(action,2));


%% 1) Features Extraction:
    if (HOG_)
        display 'HOGs Extraction, in progress... '
        run('./Sources/ExtractionHOG.m')
        %load('./Descriptors/DescripteurHOG.mat');
        fprintf(1, 'Extraction End!\n');
        fprintf(1, '------------------------------------------------------------\n\n');            
    end
    
    if(HOF_)
        display 'HOFs Extraction, in progress... '
        run('./Sources/ExtractionHOF.m')
        %load('./Descriptors/DescripteurHOF.mat');
        fprintf(1, 'Extraction End!\n');
        fprintf(1, '------------------------------------------------------------\n\n');        
    end     
    
    
    if(HOG_HOF_)
        display 'HOGs/HOFs Extraction, in progress... '
        run('./Sources/Extraction_HOG_HOF.m')
        %load('./Descriptors/DescripteurHOGHOF.mat');
        fprintf(1, 'Extraction End!\n');
        fprintf(1, '------------------------------------------------------------\n\n');
    end     
    
     
 %% 2) Projection on the space PCA
    %%%% PCA Application on the reference data
    display 'Principal Component Analysis (PCA), in progress...'
    PCA
    fprintf(1, 'PCA End! \n');
    fprintf(1, '------------------------------------------------------------\n\n');
    
%% 3) Apprentissage des HMM
    fprintf(1, 'HMMs Training, in progress... \n \n');
    Training
    fprintf(1, 'HMMs Training End! \n');
    fprintf(1, '------------------------------------------------------------\n\n');
 
%% 4) Test sur la base Weizmann
    display 'Test , in progress... '
    Test
    fprintf(1, '------------------------------------------------------------\n\n');
    